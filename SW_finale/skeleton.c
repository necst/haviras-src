/*
 * Copyright 2016 Pierandrea Cancian, Guido Walter Di Donato, Marco Bottino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define ROI_DIM 256

typedef struct 
{
    char magic[2];
    int  dim[2]; // dim[0]->cols, dim[1]-> rows
     char  data[ROI_DIM][ROI_DIM]; // 1 is black, 0 is white

}PBM_ROI;

int load_PBM_ROI (char *filename, PBM_ROI *img) {

    FILE *fp;
    int i,j;
    fp = fopen(filename, "r");
    if(fp == NULL) {
        printf("\nError: Failed to open file in read mode\n");
        return 1;
    }


    fscanf (fp, "%s\n", img->magic);
    if ( strcmp(img->magic, "P1")!=0 ) {
        printf("\nError: wrong file type\n");
        return 2;
    }


    fscanf (fp, "%d %d\n", &(img->dim[0]), &(img->dim[1]));
    if (img->dim[0]!=ROI_DIM || img->dim[1]!=ROI_DIM) {
        printf("\nError: wrong image size\n");
        return 3;
    }

    int pix;
    for (i = 0; i < img->dim[1]; i++)
        for (j = 0; j < img->dim[0]; j++){
            fscanf (fp, "%d ", &pix);
            img->data[i][j] = ( char) pix;
        }
    fclose(fp);
    return 0;
}


int save_PBM_ROI (char *filename, PBM_ROI img) {

    FILE *fp;
    int i,j;
    fp = fopen(filename, "w");
    if(fp == NULL) {
        printf("\nError: Failed to open file in write mode\n");
        return 1;
    }

    fprintf (fp, "%s\n", img.magic);
    fprintf (fp, "%d %d\n", img.dim[0], img.dim[1]);

    for (i = 0; i < img.dim[1]; i++){
        for (j = 0; j < img.dim[0]; j++)
            fprintf (fp, "%d\t", img.data[i][j]);
        fprintf (fp, "\n");
    }

    fclose(fp);
    return 0;
}


char* concat(char *s1, char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

void neighbours( char IN[ROI_DIM][ROI_DIM], int x, int y, int out[8]){

    out[0] = IN[y][x-1];
    out[1] = IN[y-1][x-1];
    out[2] = IN[y-1][x];
    out[3] = IN[y-1][x+1];
    out[4] = IN[y][x+1];
    out[5] = IN[y+1][x+1];
    out[6] = IN[y+1][x];
    out[7] = IN[y+1][x-1];

}

int main( int argc, char *argv[] )  {

	int ZS1[34][8]= {{0, 0, 0, 0, 0, 0, 1, 1},
 					 {0, 0, 0, 0, 0, 1, 1, 0},
 					 {0, 0, 0, 0, 0, 1, 1, 1},
 					 {0, 0, 0, 0, 1, 1, 0, 0},
 					 {0, 0, 0, 0, 1, 1, 1, 0},
 					 {0, 0, 0, 0, 1, 1, 1, 1},
 					 {0, 0, 0, 1, 1, 0, 0, 0},
 					 {0, 0, 0, 1, 1, 1, 0, 0},
 					 {0, 0, 0, 1, 1, 1, 1, 0},
 					 {0, 0, 0, 1, 1, 1, 1, 1},
 					 {0, 0, 1, 1, 0, 0, 0, 0},
 					 {0, 0, 1, 1, 1, 0, 0, 0},
 					 {0, 0, 1, 1, 1, 1, 0, 0},
 					 {0, 1, 1, 0, 0, 0, 0, 0},
 					 {0, 1, 1, 1, 0, 0, 0, 0},
 					 {0, 1, 1, 1, 1, 0, 0, 0},
 					 {0, 1, 1, 1, 1, 1, 0, 0},
 					 {1, 0, 0, 0, 0, 0, 0, 1},
 					 {1, 0, 0, 0, 0, 0, 1, 1},
 					 {1, 0, 0, 0, 0, 1, 1, 1},
 					 {1, 0, 0, 0, 1, 1, 1, 1},
 					 {1, 0, 0, 1, 1, 1, 1, 1},
 					 {1, 1, 0, 0, 0, 0, 0, 0},
 					 {1, 1, 0, 0, 0, 0, 0, 1},
 					 {1, 1, 0, 0, 0, 0, 1, 1},
 					 {1, 1, 0, 0, 0, 1, 1, 1},
 					 {1, 1, 0, 0, 1, 1, 1, 1},
 					 {1, 1, 1, 0, 0, 0, 0, 0},
 					 {1, 1, 1, 0, 0, 0, 0, 1},
 					 {1, 1, 1, 0, 0, 0, 1, 1},
 					 {1, 1, 1, 0, 0, 1, 1, 1},
 					 {1, 1, 1, 1, 0, 0, 0, 0},
 					 {1, 1, 1, 1, 0, 0, 0, 1},
 					 {1, 1, 1, 1, 0, 0, 1, 1}};

 	

 	int ZS2[34][8] = {{0, 0, 0, 0, 0, 0, 1, 1},
 					  {0, 0, 0, 0, 0, 1, 1, 0},
 					  {0, 0, 0, 0, 0, 1, 1, 1},
 					  {0, 0, 0, 0, 1, 1, 0, 0},
 					  {0, 0, 0, 0, 1, 1, 1, 0},
 					  {0, 0, 0, 0, 1, 1, 1, 1},
 					  {0, 0, 0, 1, 1, 0, 0, 0},
 					  {0, 0, 0, 1, 1, 1, 0, 0},
 					  {0, 0, 0, 1, 1, 1, 1, 0},
 					  {0, 0, 0, 1, 1, 1, 1, 1},
 					  {0, 0, 1, 1, 0, 0, 0, 0},
 					  {0, 0, 1, 1, 1, 0, 0, 0},
 					  {0, 0, 1, 1, 1, 1, 0, 0},
 					  {0, 0, 1, 1, 1, 1, 1, 0},
 					  {0, 0, 1, 1, 1, 1, 1, 1},
 					  {0, 1, 1, 0, 0, 0, 0, 0},
 					  {0, 1, 1, 1, 0, 0, 0, 0},
 					  {0, 1, 1, 1, 1, 0, 0, 0},
 					  {0, 1, 1, 1, 1, 1, 0, 0},
 					  {0, 1, 1, 1, 1, 1, 1, 0},
 					  {1, 0, 0, 0, 0, 0, 0, 1},
 					  {1, 0, 0, 0, 0, 0, 1, 1},
 					  {1, 0, 0, 0, 0, 1, 1, 1},
 					  {1, 1, 0, 0, 0, 0, 0, 0},
 					  {1, 1, 0, 0, 0, 0, 0, 1},
 					  {1, 1, 0, 0, 0, 0, 1, 1},
 					  {1, 1, 0, 0, 0, 1, 1, 1},
 					  {1, 1, 1, 0, 0, 0, 0, 0},
 					  {1, 1, 1, 0, 0, 0, 0, 1},
 					  {1, 1, 1, 1, 0, 0, 0, 0},
 					  {1, 1, 1, 1, 0, 0, 0, 1},
 					  {1, 1, 1, 1, 1, 0, 0, 0},
 					  {1, 1, 1, 1, 1, 0, 0, 1},
 					  {1, 1, 1, 1, 1, 1, 0, 0}};


	PBM_ROI image;

   	char* filename1 = concat(argv[1], ".pbm");
	printf("%s\n", filename1);
	load_PBM_ROI(filename1, &image);

	free(filename1); //deallocate the string

	int check[8] = {0};
    int flag1=1;
    int flag2=1; 
    int i,j,k;
    int acc=0;

    while ( flag1 || flag2 ) {


    	flag1=0;
    	acc=0;
	    for (i = 1; i < ROI_DIM-1; i++)
	        for (j = 1; j < ROI_DIM-1; j++) 
	        	if (image.data[i][j] == 1) {
		            neighbours(image.data,j,i,check);
		            for (k = 0; k < 34; k++){
		                if ((abs(check[0]) == ZS1[k][0]) && (abs(check[1]) == ZS1[k][1]) && (abs(check[2]) == ZS1[k][2]) && (abs(check[3])== ZS1[k][3]) && (abs(check[4]) == ZS1[k][4]) && (abs(check[5]) == ZS1[k][5]) && (abs(check[6]) == ZS1[k][6]) && (abs(check[7]) == ZS1[k][7])) {
		                    image.data[i][j]=-1;
		                    flag1=1;
		                    acc++;
		                    //break;
						}
		            }
	        	}

		for (i=0; i<ROI_DIM; i++)
	        for (j=0; j<ROI_DIM; j++)
	        	if (image.data[i][j]==-1)
	        		image.data[i][j]=0;
	    flag2=0;
	    acc=0;
	    for (i = 1; i < ROI_DIM-1; i++)
	        for (j = 1; j < ROI_DIM-1; j++) 
	        	if (image.data[i][j] == 1) {
		            neighbours(image.data,j,i,check);
		            for (k = 0; k < 34; k++){
		                if ((abs(check[0]) == ZS2[k][0]) && (abs(check[1]) == ZS2[k][1]) && (abs(check[2]) == ZS2[k][2]) && (abs(check[3])== ZS2[k][3]) && (abs(check[4]) == ZS2[k][4]) && (abs(check[5]) == ZS2[k][5]) && (abs(check[6]) == ZS2[k][6]) && (abs(check[7]) == ZS2[k][7])) {
		                    image.data[i][j]=-1;
		                    flag2=1;
		                    acc++;
		                    //break;
		                }
		            }
	        	}
		for (i=0; i<ROI_DIM; i++)
			for (j=0; j<ROI_DIM; j++)
				if (image.data[i][j]!=1)
					image.data[i][j]=0;
   	}
	
   	char* filename2 = concat(argv[1], "skel.pbm");
	
	save_PBM_ROI(filename2, image);
	
	free(filename2);//deallocate the string

    return 0;

}
