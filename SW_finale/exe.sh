#!/bin/bash 

gcc skeleton.c -o ske -Ofast -std=c99

for i in *.jpg; do
	python haviras.py "$i"
done

mkdir risultati
for i in *.pbm; do
	mv "$i" risultati
done
