/*
 * Copyright 2016 Pierandrea Cancian, Guido Walter Di Donato, Marco Bottino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define ROI_DIM 256

typedef struct 
{
    char magic[2];
    int  dim[2]; // dim[0]->cols, dim[1]-> rows
    char  data[ROI_DIM][ROI_DIM]; // 1 is black, 0 is white

}PBM_ROI;

int load_PBM_ROI (char *filename, PBM_ROI *img) {

    FILE *fp;
    int i,j;
    fp = fopen(filename, "r");
    if(fp == NULL) {
        printf("\nError: Failed to open file in read mode\n");
        return 1;
    }


    fscanf (fp, "%s\n", img->magic);
    if ( strcmp(img->magic, "P1")!=0 ) {
        printf("\nError: wrong file type\n");
        return 2;
    }


    fscanf (fp, "%d %d\n", &(img->dim[0]), &(img->dim[1]));
    if (img->dim[0]!=ROI_DIM || img->dim[1]!=ROI_DIM) {
        printf("\nError: wrong image size\n");
        return 3;
    }

    int pix;
    for (i = 0; i < img->dim[1]; i++)
        for (j = 0; j < img->dim[0]; j++){
            fscanf (fp, "%d ", &pix);
            img->data[i][j] = ( char) pix;
        }
    fclose(fp);
    return 0;
}


int save_PBM_ROI (char *filename, PBM_ROI img) {

    FILE *fp;
    int i,j;
    fp = fopen(filename, "w");
    if(fp == NULL) {
        printf("\nError: Failed to open file in write mode\n");
        return 1;
    }

    fprintf (fp, "%s\n", img.magic);
    fprintf (fp, "%d %d\n", img.dim[0], img.dim[1]);

    for (i = 0; i < img.dim[1]; i++){
        for (j = 0; j < img.dim[0]; j++)
            fprintf (fp, "%d\t", img.data[i][j]);
        fprintf (fp, "\n");
    }

    fclose(fp);
    return 0;
}


char* concat(char *s1, char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}
/*
void neighbours( char IN[ROI_DIM][ROI_DIM], int x, int y, int out[8]){

    out[0] = IN[y][x-1];
    out[1] = IN[y-1][x-1];
    out[2] = IN[y-1][x];
    out[3] = IN[y-1][x+1];
    out[4] = IN[y][x+1];
    out[5] = IN[y+1][x+1];
    out[6] = IN[y+1][x];
    out[7] = IN[y+1][x-1];

}
*/

void neighboursw(unsigned char IN[ROI_DIM][ROI_DIM], int i, int j, int out[8]){

    out[0] = IN[i][j-1];
    out[1] = IN[i-1][j-1];
    out[2] = IN[i-1][j];
    out[3] = IN[i-1][j+1];
    out[4] = IN[i][j+1];
    out[5] = IN[i+1][j+1];
    out[6] = IN[i+1][j];
    out[7] = IN[i+1][j-1];

}


int sum_neighbtwosix(unsigned char a[ROI_DIM][ROI_DIM], int i, int j){
    int sum=0;
    int h, k;
    for (h=-1; h<=1; ++h)
        for (k=-1; k<=1; ++k)
            sum += a[i+h][j+k];
    sum -= a[i][j];
    if (2<=sum && sum<=6) return 1; 
    else return 0;
}

int circ_transitions_neighbours(unsigned char a[ROI_DIM][ROI_DIM], int i, int j){ //prolisso as fuck
    int transitions=0;
    int neigh[8];
    int k;
    neighboursw(a, i, j, neigh);
    
    for (k = 1; k < 8; ++k)
        if (neigh[k] != neigh[k-1])
            transitions++;

    if (transitions==1 || transitions==2) return 1;   
    else return 0;
}

int products1(unsigned char a[ROI_DIM][ROI_DIM], int i, int j){
    if((a[i-1][j] * a[i+1][j] * a[i][j+1]) == 0)
        return 1;
    else return 0;
}

int products2(unsigned char a[ROI_DIM][ROI_DIM], int i, int j){
    if((a[i+1][j] * a[i][j+1] * a[i][j-1]) == 0)
        return 1;
    else return 0;
}

int products3(unsigned char a[ROI_DIM][ROI_DIM], int i, int j){
    if((a[i-1][j] * a[i][j-1] * a[i][j+1]) == 0)
        return 1;
    else return 0;
}

int products4(unsigned char a[ROI_DIM][ROI_DIM], int i, int j){
    if((a[i+1][j] * a[i][j-1] * a[i-1][j]) == 0)
        return 1;
    else return 0;

}

int ZS (unsigned char inout[ROI_DIM][ROI_DIM]){ //returns the number of iterations
    unsigned char temp[ROI_DIM][ROI_DIM];
    int NoI=0;
    int i, j;
    int changing1, changing2;

    do
    {
        NoI++;
        changing1=0;
        changing2=0;

        for (i=1; i<ROI_DIM-1; i++)                                                      
            for (j=1; j<ROI_DIM-1; j++){ 
                                             
                if(     inout[i][j]==1                             &&        
                        sum_neighbtwosix(inout, i, j)              &&    
                        circ_transitions_neighbours(inout, i, j)   &&    
                        products1(inout, i, j)                     &&    
                        products2(inout, i, j))
                {                            
                    temp[i][j]=0;                                                        
                    changing1=1;}                                                        
                else                                                                     
                    temp[i][j]=inout[i][j];                                              
            }                                                                            

        for (i=1; i<ROI_DIM-1; i++)                                                      
            for (j=1; j<ROI_DIM-1; j++){ 

                if(     temp[i][j]==1                              &&    
                        sum_neighbtwosix(temp, i, j)               && 
                        circ_transitions_neighbours(temp, i, j)    && 
                        products3(temp, i, j)                      && 
                        products4(temp, i, j))
                { 
                    inout[i][j]=0;                                                       
                    changing2=1;}                                                        
                else                                                                     
                    inout[i][j]=temp[i][j];                                              
            }                                                                            

    } while (changing1 || changing2);
    return NoI;
}

int main( int argc, char *argv[] )  {

	PBM_ROI image;

   	char* filename1 = concat(argv[1], ".pbm");
	printf("%s\n", filename1);
	load_PBM_ROI(filename1, &image);

	free(filename1); //deallocate the string

    int i,j,k;

    ZS(image.data);
	
   	char* filename2 = concat(argv[1], "skel.pbm");
	
	save_PBM_ROI(filename2, image);
	
	free(filename2);//deallocate the string

    return 0;

}
