 # Copyright 2016 Pierandrea Cancian, Guido Walter Di Donato, Marco Bottino
 # 
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 # 
 # 	http://www.apache.org/licenses/LICENSE-2.0
 # 
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import numpy as np
import cv2
import math
import sys
import time
from subprocess import call

#-------------------FUNCTIONS-------------------#

def RoiTransitions(img, r, c, columns):
	acc=0

	for j in xrange (c,columns-1):
		if img.item(r,j+1)!=img.item(r,j):
			acc += 1

	if acc>=6:
		return True
	else:
		return False


def RoiMinimum (img, r, c):
	a=0
	while 1:
		if img.item(r+1,c) == 0:
			r+=1
			a=0
		elif a==5:
			return [r,c]
		elif img.item(r,c+1) == 0:
			c+=1
			a+=1
		elif img.item(r,c-1) == 0:
			c-=1
			a+=1
		else:
			return [r,c]


def distance2points (x1,x2,y1,y2):
	return (pow(x1-x2,2) + pow(y1-y2,2)) ** (0.5)


def angle (x1,x2,y1,y2):
	return math.atan2((y1-y2),(x1-x2))

#---------------PRE PROCESSING------------------#

start_time = time.time()

image = cv2.imread(str(sys.argv[1]),0)
img = image.copy()

print 'Preprocessing'

img = cv2.GaussianBlur(img,(31,31),0)
ret, img = cv2.threshold(img,50,255,cv2.THRESH_BINARY)

timePP = time.time()
etime = timePP - start_time
print '\tTIME:', etime, 's'


#----------------ROI EXTRACTION-----------------#

print 'Roi Extraction' 
print '\tROI location'

rows, columns = img.shape


pop=0
for i in xrange(0,rows):
	for j in xrange(0, columns):
		if img.item(i,j)==255:
			if RoiTransitions (img,i,j,columns):				
				pop=1
				k=i
				break
	if pop:
		break  

k+=15
minima = []
acc=0
for i in xrange (1,columns):
	if ((img.item(k,i)==0) and (img.item(k,i-1)==255) and acc!=3):	
		minima.append(RoiMinimum(img, k, i))
		acc+=1

p1 = minima[0][:]
p2 = minima[2][:]

timeRL = time.time()
etime = timeRL - timePP
print '\t\ttime:', etime, 's'

print '\trotation...'

ang=angle(p2[1],p1[1],p2[0],p1[0])									#calculate rotation angle		

M = cv2.getRotationMatrix2D((p1[1],p1[0]),((ang*360)//(2*math.pi)),1)

#Keypoints
bordersize= math.floor(distance2points(p2[1],p1[1],p2[0],p1[0]))
bordersize= math.floor(bordersize*1.2)
q1 = p1[:]
q1[0] += 0.15*bordersize
q1[0]=math.floor(q1[0])
q2 = q1[:]
q2[0] += bordersize
q3 = q1[:]
q3[1] += bordersize
q4 = q3[:]
q4[0] += bordersize

img = cv2.warpAffine(image, M, (columns,rows) )						#Rotation of original image

timeR = time.time()
etime = timeR - timeRL 
print '\t\ttime:', etime, 's'

print '\tresize...'

img = img[q1[0]:q2[0], q1[1]:q3[1]]									#slicing the Region Of Interest

a = np.zeros((256,256), np.uint8) 
if q2[0]-q1[0] < 256:
	a= cv2.resize(img, (256,256), interpolation = cv2.INTER_CUBIC)  # _LINEAR is a more time efficient solution
elif q2[0]-q1[0] > 256:
	a= cv2.resize(img, (256,256), interpolation = cv2.INTER_AREA)	#suited in downsampling
else:
	a=img

r, c = a.shape
timeRe = time.time()
etime = timeRe - timeR
print '\t\ttime:', etime, 's'

timeROI = time.time()
etime = timeROI - timePP
print '\tTIME:', etime, 's'

print 'ROI Processing'

#----------------IMAGE PROCESSING---------------#

#Histogram Equalization

clahe = cv2.createCLAHE(clipLimit=5.5, tileGridSize=(6,6))
a = clahe.apply(a) 
print '\thistogram equalization'

#Median Filter

a = cv2.medianBlur(a,3)
a = cv2.medianBlur(a,7)
print '\tmedian filter'

#Threshold -> output in 0s and 1s. NOT 255

a = cv2.adaptiveThreshold(a,1,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY_INV,31,2.5) 
a = cv2.medianBlur(a,5)

#Erosion && Dilatation

kernel = np.ones((4,4),np.uint8)

a = cv2.dilate(a, kernel, iterations=1)
a = cv2.erode(a, kernel, iterations=2)
a = cv2.dilate(a, kernel, iterations=1)

#Padding borders with zeroes (white)
for i in xrange (0,256):
	if (i==0 or i==255):
		for j in xrange(0,256):
			a.itemset((i,j),0)
	else: 
		a.itemset((i,0),0)
		a.itemset((i,255),0)

print '\tbinarization'

#----------------SKELETONIZATION----------------#

timeIP = time.time()
etime = timeIP - timeROI
print '\tTIME:', etime, 's'

FILEout = open ('bin' + str(sys.argv[1])[:-3] + 'pbm', 'w')

FILEout.write("P1\n256 256\n")

for i in range (0, 256):
	for j in range (0, 256):
		FILEout.write(str(a.item(i,j)) + "\t")
	FILEout.write("\n")

argument = 'bin' + str(sys.argv[1])[:-4]

temp = ['./ske', argument]

call(temp)

timeSKEL = time.time()
etime = timeSKEL - timeIP
print '\tTIME:', etime, 's'

print 'TOTAL TIME:', timeSKEL - start_time, 's'
print '\n'
##END