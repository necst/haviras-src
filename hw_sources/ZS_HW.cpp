/*
 * Copyright 2016 Pierandrea Cancian, Guido Walter Di Donato, Marco Bottino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "ZS_HW.hpp"

void neighbours(ap_uint<1> IN1[ROI_DIM][ROI_DIM], ap_uint<1> IN2[ROI_DIM][ROI_DIM],ap_uint<1> IN3[ROI_DIM][ROI_DIM], ap_uint<1> IN4[ROI_DIM][ROI_DIM], int i, int j, unsigned char out[8]){
//#pragma HLS PIPELINE II=1
    out[0] = (unsigned char)IN1[i-1][j];
    out[1] = (unsigned char)IN1[i-1][j+1];
    out[2] = (unsigned char)IN2[i][j+1];
    out[3] = (unsigned char)IN2[i+1][j+1];
    out[4] = (unsigned char)IN3[i+1][j];
    out[5] = (unsigned char)IN3[i+1][j-1];
    out[6] = (unsigned char)IN4[i][j-1];
    out[7] = (unsigned char)IN4[i-1][j-1];
}

void foo(ap_uint<1> a1[ROI_DIM][ROI_DIM], ap_uint<1> a2[ROI_DIM][ROI_DIM], ap_uint<1> a3[ROI_DIM][ROI_DIM], ap_uint<1> a4[ROI_DIM][ROI_DIM], ap_uint<1> b1[ROI_DIM][ROI_DIM], ap_uint<1> b2[ROI_DIM][ROI_DIM], int i, int j) {

#pragma HLS PIPELINE II=1
    unsigned char around[8];
    unsigned char temp[7];
    unsigned char sum, tra;
#pragma HLS ARRAY_PARTITION variable=around complete
#pragma HLS ARRAY_PARTITION variable=temp complete
    
    neighbours(a1,a2,a3,a4,i,j,around);
    for (int m = 1; m < 8; ++m){
        if (around[m]!=around[m-1])
            temp[m-1] = 1;
        else
            temp[m-1] = 0;
    }

    tra = (((temp[0] + temp[1]) + (temp[2] + temp[3])) + ((temp[4] + temp[5]) + temp[6]));
    sum = (((around[0] + around[1]) + (around[2] + around[3])) + ((around[4] + around[5]) + (around[6] + around[7])));

    if ( !(around[0]&&around[2]&&around[4]) && !(around[2]&&around[4]&&around[6]) && (2<=sum && sum<=6) && (tra==1 || tra==2) ) {
        b1[i][j]=0;
        b2[i][j]=0;
    } else {
        b1[i][j]=a1[i][j];
        b2[i][j]=a2[i][j];
    } 
}

void bar(ap_uint<1> a1[ROI_DIM][ROI_DIM], ap_uint<1> a2[ROI_DIM][ROI_DIM], ap_uint<1> a3[ROI_DIM][ROI_DIM], ap_uint<1> a4[ROI_DIM][ROI_DIM], ap_uint<1> b1[ROI_DIM][ROI_DIM], ap_uint<1> b2[ROI_DIM][ROI_DIM], int i, int j) {
 
#pragma HLS PIPELINE II=1
    unsigned char around[8];
    unsigned char temp[7];
    unsigned char sum, tra;
#pragma HLS ARRAY_PARTITION variable=around complete
#pragma HLS ARRAY_PARTITION variable=temp complete
    
    neighbours(a1,a2,a3,a4,i,j,around);
    for (int m = 1; m < 8; ++m){
        if (around[m]!=around[m-1])
            temp[m-1] = 1;
        else
            temp[m-1] = 0;
    }

    tra = (((temp[0] + temp[1]) + (temp[2] + temp[3])) + ((temp[4] + temp[5]) + temp[6]));
    sum = (((around[0] + around[1]) + (around[2] + around[3])) + ((around[4] + around[5]) + (around[6] + around[7])));

    if ( !(around[0]&&around[2]&&around[6]) && !(around[0]&&around[4]&&around[6]) && (2<=sum && sum<=6) && (tra==1 || tra==2) ) {
        b1[i][j]=0;
        b2[i][j]=0;
    } else {
        b1[i][j]=a1[i][j];
        b2[i][j]=a2[i][j];
    }
    
}

int ZS_HW (volatile unsigned char * readMem, volatile unsigned char * writeMem){

#pragma HLS INTERFACE s_axilite port=return bundle=CONTROL
#pragma HLS INTERFACE m_axi depth=256 port=readMem offset=slave bundle=MEMORY
#pragma HLS INTERFACE m_axi depth=256 port=writeMem offset=slave bundle=MEMORY

	ap_uint<1>  a1[ROI_DIM][ROI_DIM];
    ap_uint<1>  a2[ROI_DIM][ROI_DIM];
    ap_uint<1>  a3[ROI_DIM][ROI_DIM];
    ap_uint<1>  a4[ROI_DIM][ROI_DIM];
    ap_uint<1>  b1[ROI_DIM][ROI_DIM];
    ap_uint<1>  b2[ROI_DIM][ROI_DIM];
    ap_uint<1>  b3[ROI_DIM][ROI_DIM];
    ap_uint<1>  b4[ROI_DIM][ROI_DIM];

	unsigned char in[ROI_DIM*ROI_DIM/8];
	unsigned char out[ROI_DIM][ROI_DIM];
    int i=0; int j=0; int k=0; int l=0;

	memcpy(in, (unsigned char *)(readMem), (256*256/8) * sizeof(unsigned char));
	
	for (int i = 0; i < ROI_DIM*ROI_DIM/8; ++i){
		for (int c = 7; c >= 0; c--){
		    k = in[i] >> c;
			
		    if (k & 1){
                a1[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 1;
                a2[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 1;
				a3[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 1;
                a4[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 1;
		    } else {
		    	a1[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 0;
                a2[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 0;
                a3[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 0;
                a4[(i*8 + 7-c)/ROI_DIM][(i*8 + 7-c)%ROI_DIM] = 0;
            }
		}
	}

	for(l=0;l<20; ++l){

        l1:for (i=0; i<ROI_DIM; i++)
            l2:for (j=0; j<ROI_DIM; j++){
              	if ( i==0 || j==0 || i==255 || j==255 ){
                	b1[i][j] = a1[i][j];
                    b2[i][j] = a2[i][j];
                    b3[i][j] = a3[i][j];
                    b4[i][j] = a4[i][j];
              	} else {
                	foo(a1, a2, a3, a4, b1, b2, i, j);
                    b3[i][j] = b1[i][j];
                    b4[i][j] = b2[i][j];
                }
            }

        l5:for (i=0; i<ROI_DIM; i++)
            l6:for (j=0; j<ROI_DIM; j++){
              	if ( i==0 || j==0 || i==255 || j==255 ){
               		a1[i][j] = b1[i][j];
                    a2[i][j] = b2[i][j];
                    a3[i][j] = b3[i][j];
                    a4[i][j] = b4[i][j];
            	} else {
                	bar(b1, b2, b3, b4, a1, a2, i, j);
                    a4[i][j] = a2[i][j];
                    a3[i][j] = a1[i][j];
                }
            }
    }

	for(i=0; i<ROI_DIM; ++i)
		for(j=0; j<ROI_DIM; ++j)
			out[i][j] = (unsigned char) a1[i][j];
	memcpy((unsigned char *)(writeMem), out, ROI_DIM*ROI_DIM * sizeof(unsigned char));
	return (long)readMem;
}
