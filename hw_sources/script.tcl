############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2016 Xilinx, Inc. All Rights Reserved.
############################################################
open_project ZS_Zed
set_top ZS_HW
add_files ZS_HW.cpp
add_files ZS_HW.hpp
add_files -tb MAINAXI.cpp
add_files -tb preskel.pbm
open_solution "solution1"
set_part {xc7z020clg484-1} -tool vivado
create_clock -period 10 -name default
csim_design
csynth_design
cosim_design -setup
export_design -format ip_catalog
