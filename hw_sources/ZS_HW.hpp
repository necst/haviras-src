/*
 * Copyright 2016 Pierandrea Cancian, Guido Walter Di Donato, Marco Bottino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "ap_axi_sdata.h"
#include <string.h>

//////////////////////
#define ROI_DIM 256///
//////////////////////

void neighbours(ap_uint<1> IN1[ROI_DIM][ROI_DIM], ap_uint<1> IN2[ROI_DIM][ROI_DIM],ap_uint<1> IN3[ROI_DIM][ROI_DIM], ap_uint<1> IN4[ROI_DIM][ROI_DIM], int i, int j, unsigned char out[8]);

void foo(ap_uint<1> a1[ROI_DIM][ROI_DIM], ap_uint<1> a2[ROI_DIM][ROI_DIM], ap_uint<1> a3[ROI_DIM][ROI_DIM], ap_uint<1> a4[ROI_DIM][ROI_DIM], ap_uint<1> b1[ROI_DIM][ROI_DIM], ap_uint<1> b2[ROI_DIM][ROI_DIM], int i, int j);

void bar(ap_uint<1> a1[ROI_DIM][ROI_DIM], ap_uint<1> a2[ROI_DIM][ROI_DIM], ap_uint<1> a3[ROI_DIM][ROI_DIM], ap_uint<1> a4[ROI_DIM][ROI_DIM], ap_uint<1> b1[ROI_DIM][ROI_DIM], ap_uint<1> b2[ROI_DIM][ROI_DIM], int i, int j);

int ZS_HW (volatile unsigned char * readMem, volatile unsigned char * writeMem);
