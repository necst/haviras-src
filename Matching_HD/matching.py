 # Copyright 2016 Pierandrea Cancian, Guido Walter Di Donato, Marco Bottino
 # 
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 # 
 # 	http://www.apache.org/licenses/LICENSE-2.0
 # 
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

from os import listdir
from os.path import isfile, join
from mlib import *

mypath = "./tests"

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

onlyfiles.sort()

matrix = [[str(MHD(minutiae(join(mypath, x)), minutiae(join(mypath, y)))) if (x>=y) else '0' for x in onlyfiles] for y in onlyfiles]

fileName = "output.csv"

f = open(fileName, "wb")
f.write("\t")
f.write("\t".join(onlyfiles))
f.write("\n")
for row in matrix:
	f.write("-\t")
	f.write("\t".join(row))
	f.write("\n")
f.close()
