 # Copyright 2016 Pierandrea Cancian, Guido Walter Di Donato, Marco Bottino
 # 
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 # 
 # 	http://www.apache.org/licenses/LICENSE-2.0
 # 
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.

import numpy as np
import math
import sys
import cv2
#---------------FUNZIONI--------------#

def variation (a,i,j):
	k = 0

	if a.item(i-1,j-1) != a.item(i,j-1):
		k += 1
	if a.item(i,j-1) != a.item(i+1,j-1):
		k += 1
	if a.item(i+1,j-1) != a.item(i+1,j):
		k += 1
	if a.item(i+1,j) != a.item(i+1,j+1):
		k += 1
	if a.item(i+1,j+1) != a.item(i,j+1):
		k += 1
	if a.item(i,j+1) != a.item(i-1,j+1):
		k += 1
	if a.item(i-1,j+1) != a.item(i-1,j):
		k += 1
	if a.item(i-1,j) != a.item(i-1,j-1):
		k += 1

	return k

def distanza (x1,x2,y1,y2):
	return (pow(x1-x2,2) + pow(y1-y2,2)) ** (0.5)

def MHD (A, B):
	acc = [0,0]
	for i in xrange(0, len(A)):
		a = A[i]
		D = [ distanza(a[0],B[j][0],a[1],B[j][1]) for j in xrange(0, len(B))]		
		acc[0] += min(D)/len(A)
		del D[:]	
		
	for i in xrange(0, len(B)):
		b = B[i]
		D = [ distanza(b[0],A[j][0],b[1],A[j][1]) for j in xrange(0, len(A))]
		acc[1] += min(D)/len(B)
		del D[:]
	return max(acc)

def minutiae(im):
	print "..."
	a = cv2.imread(im,0)
	n_endpoint = 0
	n_fork = 0
	r, c = a.shape
	endpoint = []
	fork = []
	tutti = []
	for i in range (3, r-2):
		for j in range (3, c-2):
			if a.item(i,j) == 1:

				k = variation(a,i,j)
			
				if k == 0:
					pass
				elif k == 2: #end point
					endpoint += [[i, j]]
					tutti += [[i, j]]
						
				elif k == 4: #linea
					#pass
					tutti += [[i, j]]				
				
				elif k >= 6: #biforcazione
					fork += [[i, j]]
					tutti += [[i, j]]
				
				else:
					print("Errore")
					print k
	return tutti#, endpoint, tutti

